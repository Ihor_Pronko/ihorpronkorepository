package com.personal.ihor.service.io;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertTrue;

public class IOFileTest {

    private static String fileWithText = "src/test/resources/IOTestFile.txt";
    private static String testedText = "tested text";

    @Test
    public void writeToFileTest() throws IOException {
        IOFile.writeToFile(fileWithText, testedText, false);
        Assert.assertEquals(testedText, IOFile.readFromFile(fileWithText));
    }

    @Test
    public void writeToFileAssistTest() throws IOException {
        File writeToFileAssist = new File(fileWithText);
        Assert.assertEquals(IOFile.writeToFileAssist(writeToFileAssist, testedText), true);
    }

    @Test (expected = IOException.class)
    public void testReadFromIOException() throws IOException {
        String notExistFile = "src/test/resources/FileNotExists";
        IOFile.readFromFile(notExistFile);
    }

    @Test
    public void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<IOFile> constructor = IOFile.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);
        constructor.newInstance();
    }

    @AfterClass
    public static void deleteFiles() {
        new File(fileWithText).delete();
        new File(fileWithText.replaceAll("[/][^/]+$", "")).delete();
    }
}
