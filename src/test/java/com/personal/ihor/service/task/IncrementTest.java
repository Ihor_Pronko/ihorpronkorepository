package com.personal.ihor.service.task;

import org.junit.Test;

/**
 */

public class IncrementTest {

    @Test
    public void test() throws InterruptedException {
        IncrementSynchronizee inc = new IncrementSynchronizee(0);
        IncrementThread incrementThread = new IncrementThread("ThreadOne", inc);
        incrementThread.start();
        Thread.sleep(10);
        System.out.println(inc.getValue());
    }

    @Test
    public void testTwo() throws InterruptedException {
        IncrementSynchronizee inc = new IncrementSynchronizee(0);
        IncrementThreadTwo incrementThreadTwo = new IncrementThreadTwo("ThreadOne", inc);
        Thread tread = new Thread(incrementThreadTwo);
        tread.start();
        Thread.sleep(10);
        System.out.println(inc.getValue());
    }
}
