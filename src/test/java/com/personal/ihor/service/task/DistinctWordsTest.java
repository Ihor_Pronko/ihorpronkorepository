package com.personal.ihor.service.task;

import com.personal.ihor.service.io.IOFile;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DistinctWordsTest {

    private static String filePath = "src/test/resources/FileWithText.txt";
    private static String textToFile = "\"I\", \"can\" \"catch-up\" 2312  !@2$#$%^&* %^7 %^4  ^$ 644 ^ 4%5 \"\"";
    private DistinctWords distinct = new DistinctWords();
    private static List<String> wordsList = new ArrayList<>();

    @BeforeClass
    public static void before() {
        IOFile.writeToFile(filePath, textToFile, false);
        wordsList.add("I");
        wordsList.add("can");
        wordsList.add("catch-up");
    }

    @Test
    public void distinctWordsTest() {
        Assert.assertEquals(distinct.distinctWords(filePath), wordsList);
    }

    @AfterClass
    public static void after() {
        new File(filePath).delete();
    }
}