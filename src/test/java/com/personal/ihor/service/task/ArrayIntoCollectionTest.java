package com.personal.ihor.service.task;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class ArrayIntoCollectionTest {

    Integer[] arrayInt = {1, 2};

    @Test
    public void testArrayList() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        ArrayList<Integer> arrayListTwo = new ArrayList<>();
        arrayListTwo.add(1);
        arrayListTwo.add(2);
        Assert.assertEquals(ArrayIntoCollection.addToCollection(arrayInt, arrayList), arrayListTwo);
    }

    @Test
    public void testLinkedList() {
        LinkedList<Integer> linkedList = new LinkedList<>();
        LinkedList<Integer> linkedListTwo = new LinkedList<>();
        linkedListTwo.add(1);
        linkedListTwo.add(2);
        Assert.assertEquals(ArrayIntoCollection.addToCollection(arrayInt, linkedList), linkedListTwo);
    }

    @Test
    public void testStack() {
        Stack<Integer> stack = new Stack<>();
        Stack<Integer> stackTwo = new Stack<>();
        stackTwo.add(1);
        stackTwo.add(2);
        Assert.assertEquals(ArrayIntoCollection.addToCollection(arrayInt, stack), stackTwo);
    }

    @Test
    public void testHashSet() {
        HashSet<Integer> hashSet = new HashSet<>();
        HashSet<Integer> hashSetTwo = new HashSet<>();
        hashSetTwo.add(1);
        hashSetTwo.add(2);
        Assert.assertEquals(ArrayIntoCollection.addToCollection(arrayInt, hashSet), hashSetTwo);
    }

    @Test
    public void testLinkedHashSet() {
        LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>();
        LinkedHashSet<Integer> linkedHashSetTwo = new LinkedHashSet<>();
        linkedHashSetTwo.add(1);
        linkedHashSetTwo.add(2);
        Assert.assertEquals(ArrayIntoCollection.addToCollection(arrayInt, linkedHashSet), linkedHashSetTwo);
    }
}
