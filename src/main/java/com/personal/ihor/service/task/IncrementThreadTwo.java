package com.personal.ihor.service.task;

/**
 * Created by ihor on 20.06.17.
 */
public class IncrementThreadTwo implements Runnable {

    private String threadName;

    private IncrementSynchronizee incrementSynchronize;

    public IncrementThreadTwo(String threadName, IncrementSynchronizee incrementSynchronize) {
        this.threadName = threadName;
        this.incrementSynchronize = incrementSynchronize;
    }

    @Override
    public void run() {
        incrementSynchronize.setValue(incrementSynchronize.getNextValue());
    }
}