package com.personal.ihor.service.task;


/**
 * Util class task #3.
 */
public class IncrementSynchronizee {

    public IncrementSynchronizee(int value) {
        this.value = value;
    }

    public int value;

    synchronized int getNextValue() {
        return value + 1;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
