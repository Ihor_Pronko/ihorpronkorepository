package com.personal.ihor.service.task;

import com.personal.ihor.service.io.IOFile;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Util class to distinct words from a text file.
 */
public class DistinctWords {

    /**
     * Method find words in text file and create a List of this words.
     * @param filePlace the path to the file.
     * @return list of words.
     */
    public List<String> distinctWords(String filePlace) {
        String text = IOFile.readFromFile(filePlace);
        List<String> answer = new ArrayList<>();

        Pattern pattern = Pattern.compile("[A-ZА-Яa-zа-яІіЇїЄє]+[\'\\-]*[a-z]*");
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            answer.add(matcher.group());
        }
        return answer;
    }
}
