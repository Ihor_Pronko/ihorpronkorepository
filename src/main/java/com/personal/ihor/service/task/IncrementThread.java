package com.personal.ihor.service.task;

/**
 *
 */
public class IncrementThread extends Thread {

    private IncrementSynchronizee incrementSynchronize;

    public IncrementThread(String threadName, IncrementSynchronizee incrementSynchronize) {
        super(threadName);
        this.incrementSynchronize = incrementSynchronize;
    }

    @Override
    public void run() {
        incrementSynchronize.setValue(incrementSynchronize.getNextValue());
    }
}

