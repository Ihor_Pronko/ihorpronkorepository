package com.personal.ihor.service.task;

import java.util.Arrays;
import java.util.Collection;

/**
 * Util class to task #2.
 */
public final class ArrayIntoCollection {

    private ArrayIntoCollection() {}

    /**
     * Method add array to collection.
     * @param array need to be added.
     * @param collection in which an array is added.
     * @param <T> type of objects in collection and array.
     * @return collection type T.
     */
    public  static <T> Collection<T> addToCollection(T[] array, Collection<T> collection) {
        collection.addAll(Arrays.asList(array));
        return collection;
    }
}
